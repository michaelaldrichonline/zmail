extends VBoxContainer

var visible_count = 0
const max_visible = 30
const INBOX_BUTTON = preload("InboxZmail.tscn")
export var state_path : NodePath
onready var player_state = get_node(state_path)

var dragging = false

var drag_start_pos
var drag_id = 0
var last_drag_distance = 0.0
# Called when the node enters the scene tree for the first time.
func _ready():
	# get_child(0).connect("pressed", self, "make_it_better")
	player_state.connect("new_mail", self, "make_a_button")
	player_state.connect("trash_mail", self, "on_trash")
	visible_count = get_child_count()

func _process(delta):
	if dragging:
		apply_drag(last_drag_distance)

func drag_distance(event):
	last_drag_distance = event.position.x - drag_start_pos.x
	return last_drag_distance

func _input(event):
# func _unhandled_input(event):
	# If we aren't dragging, ignore all events outside our rect.
	if not (dragging or self.get_global_rect().has_point(event.position)):
		return
	if event is InputEventMouseButton or event is InputEventScreenTouch:
		if event is InputEventMouseButton:
			if event.button_index != BUTTON_LEFT:
				# Only process left button click/unclick
				return
		dragging = event.is_pressed()
		if dragging:
			drag_start_pos = event.position
			# Calculate what child to display drag on by finding how far down our VBox the click
			# was, then dividing that by the size of zmails + the seperation between them, and
			# we get a perfect id
			drag_id = int(abs(self.get_global_position().y - event.position.y) / (45 + get_constant("separation")))
		else:
			drag_id = -1
			drag_start_pos = null
	if not dragging:
		return
	if event is InputEventMouseMotion or event is InputEventScreenDrag:
		var dd = drag_distance(event)
		# print(dd)
		if not dd:
			return
		apply_drag(dd)

func apply_drag(distance):
	# for i in range(5):
	var c = get_child(drag_id)
	if not c:
		return # continue
	if distance > 0:
		c.left_drag_amount = distance
		c.right_drag_amount = 0.0
	else:
		c.right_drag_amount = abs(distance)
		c.left_drag_amount = 0.0


func make_a_button(_how_many_new_mail):
	_how_many_new_mail = min(_how_many_new_mail, 5)
	for i in range(_how_many_new_mail):
		var button = INBOX_BUTTON.instance()
		if visible_count == 0:
			button.random = false
		add_child(button)
		visible_count += 1
		move_child(button, 0)
		button.connect("pressed", self, "make_it_better")
		if visible_count > max_visible:
			hide_mail(10)

func on_trash(_num):
	if player_state.total < visible_count:
		hide_mail(visible_count - player_state.total)

func hide_mail(how_many):
	var last_child = get_child_count() - 1
	for i in range(how_many):
		var c = get_child(last_child-i)
		if not c:
			return
		c.queue_free()
		visible_count -= 1
		# remove_child(c)

func make_it_better():
	player_state.add_mail((randi() % 3) + 1)
	player_state.save_game()
