extends HBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var inboxes = 0
const INBOX = preload("Inbox.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	# another_inbox()
	for id in range(99):
		var save_path = "user://zmail_" + str(id) + ".zzz"
		if id == 0:
			save_path = "user://zmail.zzz"
		var save = File.new()
		if not save.file_exists(save_path):
			break
		another_inbox()
	if inboxes == 0:
		another_inbox()

func another_inbox():
	var new_inbox = INBOX.instance()
	new_inbox.connect("another_box", self, "another_inbox")
	add_child(new_inbox)
	new_inbox.player_state.set_id(inboxes)
	inboxes += 1

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
