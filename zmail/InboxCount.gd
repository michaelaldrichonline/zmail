extends Label

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var state_path : NodePath
onready var player_state = get_node(state_path)

# Called when the node enters the scene tree for the first time.
# func _ready():
# 	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if not player_state:
		return
	var count_str = str(player_state.get("total"))
	var i = count_str.length() - 3
	while i > 0:
		count_str = count_str.insert(i, ",")
		i = i - 3
	text = "Inbox - (" + count_str + ")"
