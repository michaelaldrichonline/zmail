extends Button

# Declare member variables here. Examples:
# var a = 2
onready var from_label = get_node("%from_email_label")
onready var subject_label = get_node("%subject_label")
onready var snippet_label = get_node("%snippet_label")
onready var arrive_time_label = get_node("%arrive_time_label")
onready var type_texture = get_node("%type_texture")
onready var rslide_rect = get_node("HBoxContainer/RSlideRect")
onready var lslide_rect = get_node("HBoxContainer/LSlideRect")
var press_start_pos
var last_drag_distance
var left_drag_amount = 0.0
var right_drag_amount = 0.0

var words = "Spicy jalapeno bacon ipsum dolor amet incididunt buffalo in nisi doner pork loin sed Non dolore tail, hamburger fatback beef ball tip exercitation cupidatat consectetur chuck Do consectetur andouille sed shank pork chop bresaola, turkey kevin picanha Sirloin pork chop pig proident duis turkey ut, exercitation chicken sausage flank tail nostrud fatback Et meatball pork loin, velit pig beef ut buffalo swine laborum Velit dolore venison chuck Jowl salami chicken magna dolore non, consequat pastrami pancetta commodo meatball esse burgdoggen Sausage ut sunt jerky burgdoggen fugiat, occaecat ham hock alcatra pastrami id Cupim tri-tip consequat excepteur, est pariatur nulla venison irure Nostrud ea ipsum ex venison Spare ribs adipisicing incididunt sirloin, cupim id in ex in venison sunt Kielbasa bresaola ut tail tongue consequat shankle rump anim ham alcatra labore nulla veniam Chislic laborum eu, tempor meatloaf qui ex reprehenderit turkey Anim non drumstick laboris biltong meatloaf Culpa pancetta proident, nisi kielbasa bresaola shankle laboris reprehenderit frankfurter tail in meatloaf turkey in Ex ham hock laborum, bresaola in ground round boudin tempor deserunt exercitation pork belly Ipsum venison enim strip steak sirloin Consectetur turducken turkey dolore Do laboris velit, culpa ut tri-tip filet mignon jowl labore cupim sunt chicken consequat turducken chuck Non consectetur magna in Adipisicing chuck meatball reprehenderit Incididunt kevin picanha frankfurter shank Ribeye est aliqua ea brisket short ribs strip steak meatball Ad qui landjaeger ground round brisket buffalo meatloaf hamburger veniam turducken ipsum elit ribeye Aliquip chuck eu incididunt salami Chicken ball tip frankfurter anim aliqua Rump nostrud t-bone doner enim sirloin aliqua laboris Excepteur consequat quis, prosciutto adipisicing buffalo exercitation beef ground round andouille pork belly Pancetta in turkey ball tip".split(" ")
var colors = ["#4a796e", "#796e4a", "#6e4a79"]
var word_count = len(words)
var textures = [
	preload("res://zmail/email_types/email_type_0.png"),
	preload("res://zmail/email_types/email_type_1.png"),
	preload("res://zmail/email_types/email_type_2.png"),
	preload("res://zmail/email_types/email_type_3.png"),
	preload("res://zmail/email_types/email_type_4.png"),
	preload("res://zmail/email_types/email_type_5.png"),
	preload("res://zmail/email_types/email_type_6.png"),
	preload("res://zmail/email_types/email_type_7.png"),
	preload("res://zmail/email_types/email_type_8.png"),
	preload("res://zmail/email_types/email_type_9.png"),
	preload("res://zmail/email_types/email_type_10.png"),
	preload("res://zmail/email_types/email_type_11.png"),
	preload("res://zmail/email_types/email_type_12.png"),
	# load("res:://zmail/email_types/email_type_13.png")
]
export var random = true

# func _init(be_random=true):
	# random = be_random

# Called when the node enters the scene tree for the first time.
func _ready():
	"""
	for i in range(13):
		textures.append(load("res://zmail/email_types/email_type_" + str(i) + ".png"))
	"""
	if random:
		subject_label.text = random_words(2)
		snippet_label.text = random_words(4) + "..."
		from_label.text = random_words(1) + "@" + random_words(1) + ".zzz"
		arrive_time_label.text = Time.get_time_string_from_system()
		type_texture.set_texture(textures[randi() % len(textures)])
		# var color = colors[randi() % len(colors)]
		# self["custom_styles/normal"].bg_color = Color(color)
	# self["custom_styles/normal"].bg_color = Color.from_hsv(rand_range(0.0, 1.0), 0.54, 0.74, 1.0)
	# self["custom_styles/normal"].bg_color = Color.from_hsv(rand_range(0.0, 1.0), 0.12, 0.34, 1.0)
	self.connect("button_down", self, "on_press")
	return

func on_press():
	pass

func _unhandled_input(event):
	pass

func drag_distance(event):
	if not press_start_pos:
		press_start_pos = event.position
		last_drag_distance = 0.0
		return 0.0
	last_drag_distance = event.position.x - press_start_pos.x
	return last_drag_distance

func _input(event):
	return
	if event is InputEventMouseButton or event is InputEventScreenTouch:
		if self.is_hovered():
			lslide_rect.visible = event.pressed
			rslide_rect.visible = event.pressed
		self.drag_distance(event)
	if event is InputEventMouseMotion or event is InputEventScreenDrag:
		var dd = self.drag_distance(event)
		if self.pressed and dd:
			if dd > 0:
				lslide_rect.rect_min_size.x = abs(dd)
				rslide_rect.rect_min_size.x = 0
			else:
				rslide_rect.rect_min_size.x = abs(dd)
				lslide_rect.rect_min_size.x = 0
			return
		if rslide_rect.visible:
			rslide_rect.rect_min_size.x = 0
			rslide_rect.visible = false
		if lslide_rect.visible:
			lslide_rect.rect_min_size.x = 0
			lslide_rect.visible = false

func _process(delta):
	lslide_rect.rect_min_size.x = left_drag_amount
	rslide_rect.rect_min_size.x = right_drag_amount
	right_drag_amount = 0
	left_drag_amount = 0
	return
	var decay_speed = 1545
	if right_drag_amount > 0:
		right_drag_amount = clamp(right_drag_amount - (decay_speed * delta), 0.0, 100)
	if left_drag_amount > 0:
		left_drag_amount = clamp(left_drag_amount - (decay_speed * delta), 0.0, 100)

func random_words(number, delim=" "):
	var out = ""
	for i in range(number):
		if i != 0:
			out += delim
		out += words[randi() % word_count]
	return out
