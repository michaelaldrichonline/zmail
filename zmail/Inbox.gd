extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var state_path : NodePath
onready var player_state = get_node(state_path)
signal another_box()

# Called when the node enters the scene tree for the first time.
func _ready():
	player_state.connect("another_box", self, "signal_box")
	# player_state.connect("trash_mail", self, "trashed")
	pass # Replace with function body.

# func trashed(how_many):
#	if (how_many == 1) and (player_state.id != 0):
# 		queue_free()

func signal_box():
	emit_signal("another_box")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
