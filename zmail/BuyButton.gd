extends Button


# Declare member variables here. Examples:
# var a = 2
# var BUY_WINDOW = preload("BuyWindow.tscn").instance()
export var state_path : NodePath
export var upgrade = false
export var another_box = false

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("button_up", self, "on_pressed")

func on_pressed():
	if not state_path:
		return
	var player_state = get_node(state_path)
	if another_box:
		if player_state.total < 1000:
			return
		player_state.trash_mail(1000)
		player_state.emit_signal("another_box")
		# queue_free() TODO need to save state.
		return
	if upgrade:
		if player_state.total < 100:
			return
		player_state.trash_mail(100)
		# player_state.total -= 100
		player_state.per_tick += 1
		player_state.per_tick = int(player_state.per_tick * 1.1)
		# player_state.interval = player_state.interval * 0.8
	else:
		player_state.per_tick = 0
		# player_state.interval = 1.0
		player_state.trash_mail(player_state.total)
		# player_state.total = 0
	# get_tree().get_root().add_child(BUY_WINDOW)
	# BUY_WINDOW.popup()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
