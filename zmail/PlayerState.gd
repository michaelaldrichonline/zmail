extends Node

# Declare member variables here. Examples:
# var a = 2
# var interval = 1.0
# var til_next = 0.0
var leftover = 0.0
var per_tick: int = 0
# Total is a float, you can have fractional email for the sake of easier math.
var total = 1.0
signal new_mail(new_mail_count)
signal trash_mail(trash_mail_count)
signal another_box()
export var id = -1
var save_path = "" # user://zmail_" + str(id) + ".zzz"
# var autosave = Timer.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	# til_next = interval
	if id != -1:
		load_game()

func set_id(in_id):
	id = in_id
	save_path = "user://zmail_" + str(id) + ".zzz"
	if id == 0:
		save_path = "user://zmail.zzz"
	load_game()
	# autosave.connect("timeout", self, "save_game")
	# autosave.set_autostart(true)
	# autosave.start(1.0)

func load_game():
	var save = File.new()
	if not save.file_exists(save_path):
		leftover = 0.0
		per_tick = 0
		add_mail(1)
		return
	save.open(save_path, File.READ)
	var save_data = parse_json(save.get_line())
	total = save_data["total"]
	# interval = save_data["interval"]
	per_tick = save_data["per_tick"]
	for i in range(min(25, total)):
		emit_signal("new_mail", 1)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# if we give A every B seconds, and delta is "seconds", then
	# delta * A is the amount of mail (potentially fractional) to add in this frame.
	var next = (per_tick * delta) + leftover
	var full_next = int(next)
	if full_next >= 1:
		add_mail(full_next)
	leftover = next - full_next
	"""
	til_next -= delta
	if interval < delta:
		# per_tick += 1 
		pass
	if til_next <= 0.0:
		total += per_tick
		add_mail(per_tick)
		til_next = interval
		# interval = interval * 0.9
	"""

func add_mail(new_mail):
	if new_mail <= 0:
		return
	total += new_mail
	emit_signal("new_mail", new_mail)

func trash_mail(how_many):
	if how_many <= 0:
		return
	total -= how_many
	emit_signal("trash_mail", how_many)
	if total == 0:
		add_mail(1)

# Connected to the autosave timer in the scene tree.
func save_game():
	var save_data = {
		# "interval": interval,
		"per_tick": per_tick,
		"total": total,
	}
	var save = File.new()
	save.open(save_path, File.WRITE)
	save.store_line(to_json(save_data))
	save.close()
	
